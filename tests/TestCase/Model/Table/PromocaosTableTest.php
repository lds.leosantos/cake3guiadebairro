<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PromocaosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PromocaosTable Test Case
 */
class PromocaosTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PromocaosTable
     */
    public $Promocaos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Promocaos',
        'app.Robots',
        'app.Users',
        'app.Situations',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Promocaos') ? [] : ['className' => PromocaosTable::class];
        $this->Promocaos = TableRegistry::getTableLocator()->get('Promocaos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Promocaos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
