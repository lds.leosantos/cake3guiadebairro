<?php
namespace App\Test\TestCase\View\Helper;

use App\View\Helper\SearchHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\SearchHelper Test Case
 */
class SearchHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\View\Helper\SearchHelper
     */
    public $Search;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Search = new SearchHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Search);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
