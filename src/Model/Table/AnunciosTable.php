<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Anuncios Model
 *
 * @property \App\Model\Table\RobotsTable&\Cake\ORM\Association\BelongsTo $Robots
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\AnunciosSituationsTable&\Cake\ORM\Association\BelongsTo $AnunciosSituations
 * @property \App\Model\Table\CatsAnunciosTable&\Cake\ORM\Association\BelongsTo $CatsAnuncios
 * @property \App\Model\Table\SituationsTable&\Cake\ORM\Association\BelongsToMany $Situations
 *
 * @method \App\Model\Entity\Anuncio get($primaryKey, $options = [])
 * @method \App\Model\Entity\Anuncio newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Anuncio[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Anuncio|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Anuncio saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Anuncio patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Anuncio[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Anuncio findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AnunciosTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('anuncios');
        $this->setDisplayField('titulo');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Upload');
        $this->addBehavior('UploadRed');
        $this->addBehavior('DeleteArq');
        $this->addBehavior('SlugUrl');


        $this->belongsTo('Robots', [
            'foreignKey' => 'robot_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('AnunciosSituations', [
            'foreignKey' => 'anuncios_situation_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('CatsAnuncios', [
            'foreignKey' => 'cats_anuncio_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsToMany('Situations', [
            'foreignKey' => 'anuncio_id',
            'targetForeignKey' => 'situation_id',
            'joinTable' => 'anuncios_situations',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('titulo')
            ->maxLength('titulo', 220)
            ->notEmptyString('titulo');

        $validator
            ->scalar('descricao')
            ->notEmptyString('descricao');

        $validator
            ->scalar('conteudo')
            ->notEmptyString('conteudo');

        $validator
            ->notEmptyString('imagem', 'Necessário selecionar a foto')
            ->add('imagem', 'file', [
                'rule' => ['mimeType', ['image/jpeg', 'image/png']],
                'message' => 'Extensão da foto inválida. Selecione foto JPEG ou PNG',
            ]);

        $validator
            ->scalar('slug')
            ->maxLength('slug', 220)
            ->notEmptyString('slug');

        $validator
            ->scalar('keywords')
            ->maxLength('keywords', 220)
            ->notEmptyString('keywords');

        $validator
            ->scalar('description')
            ->maxLength('description', 220)
            ->notEmptyString('description');



        $validator
            ->integer('qnt_acesso')
            ->notEmptyString('qnt_acesso');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['robot_id'], 'Robots'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['anuncios_situation_id'], 'AnunciosSituations'));
        $rules->add($rules->existsIn(['cats_anuncio_id'], 'CatsAnuncios'));

        return $rules;
    }

    public function getAnunciosUltimos()
    {
        $query =  $this->find()
            ->select(['id', 'titulo', 'imagem', 'slug'])
            ->where(['Anuncios.anuncios_situation_id =' => 1])
            ->order(['Anuncios.id' => 'DESC'])
            ->limit(5);

        return $query;
    }

    public function getAnunciosDestaque()
    {
        $query =  $this->find()
            ->select(['id', 'titulo', 'imagem', 'slug'])
            ->where(['Anuncios.anuncios_situation_id =' => 1])
            ->order(['Anuncios.qnt_acesso' => 'DESC'])
            ->limit(5);

        return $query;
    }

    public function getCatAnunciosUltimos($id = null)
    {
        $query =  $this->find()
            ->select(['id', 'titulo', 'imagem', 'slug'])
            ->where([
                'Anuncios.anuncios_situation_id =' => 1,
                'Anuncios.cats_anuncio_id = ' => $id
            ])
            ->order(['Anuncios.id' => 'DESC'])
            ->limit(5);

        return $query;
    }


    public function getCatAnunciosDestaques($id = null)
    {
        $query =  $this->find()
            ->select(['id', 'titulo', 'imagem', 'slug'])
            ->where([
                'Anuncios.anuncios_situation_id =' => 1,
                'Anuncios.cats_anuncio_id = ' => $id
            ])
            ->order(['Anuncios.qnt_acesso' => 'DESC'])
            ->limit(5);

        return $query;
    }


    public function getCatAnunciosNomeCat($id)
    {

        $query =  $this->find()
            ->select(['id', 'cats_anuncio_id'])
            ->contain(['CatsAnuncios'])
            ->where([
                'Anuncios.cats_anuncio_id = ' => $id
            ])

            ->first();

        return $query;
    }


    public function getListAnunciosDestaque()
    {
        $query =  $this->find()
            ->select(['id', 'titulo', 'descricao', 'imagem', 'slug'])
            ->where(['Anuncios.anuncios_situation_id =' => 1])
            ->order(['Anuncios.id' => 'DESC'])
            ->limit(5);

        return $query;
    }

    public function getVerAnuncio($slug)
    {
        $query =  $this->find()
            ->select(['id', 'titulo', 'conteudo', 'imagem', 'user_id' ,'created'])
            ->where([
                'Anuncios.anuncios_situation_id =' => 1,
                'Anuncios.slug =' => $slug
            ])
            ->order(['Anuncios.id' => 'ASC'])
            ->first();

        return $query;
    }
}
