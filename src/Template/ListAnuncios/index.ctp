<main role="main">
    <div class="jumbotron blog">
        <div class="container">
            <h1 class="display-4 text-center titulo-cat-home">Anúncios da Categoria <?= $catAnuncio->nome ?>
            </h1>
            <div class="row">
                <div class="col-md-8">
                    <?php if ($anuncios) { ?>
                        <?php foreach ($anuncios as $anuncio) { ?>
                            <div class="row featurette shadow bg-white p-1 mb-3">
                                <div class="col-md-7 order-md-2">
                                    <div class="anunc-title">
                                        <h2 class="featurette-heading">
                                            <?= $this->Html->link(__($anuncio->titulo), ['controller' => 'Anuncios', 'action' => 'view', $anuncio->slug]);   ?>
                                        </h2>
                                    </div>
                                    <p class="lead"> <?= $anuncio->descricao  ?>.</p>
                                </div>
                                <div class="col-md-5 order-md-1">
                                    <?php $imagem = $this->Html->image(
                                        '../files/anuncio/' . $anuncio->id . '/' . $anuncio->imagem,
                                        ['class' => 'bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto']
                                    );
                                    echo $this->Html->link(__($imagem), ['controller' => 'Anuncios', 'action' => 'view', $anuncio->slug], ['escape' => false]);
                                    ?>

                                </div>
                            </div>

                        <?php } ?>

                        <?= $this->element('paginacao_bairro'); ?>

                    <?php } else {  ?>
                        <div class="alert alert-danger" role="alert">
                            Nenhuma Anúncio encontrado na Categoria!!
                        </div>

                        <h1 class="display-4 text-center titulo-cat-home">Sugestões de Anúncios </h1>

                        <?php foreach ($anunciosListDests as $anuncioListDest) { ?>
                            <div class="row featurette shadow bg-white p-1 mb-3">
                                <div class="col-md-7 order-md-2">
                                    <div class="anunc-title">
                                        <h2 class="featurette-heading">
                                            <?= $this->Html->link(__($anuncioListDest->titulo), ['controller' => 'Anuncios', 'action' => 'view', $anuncioListDest->slug]);   ?>
                                        </h2>
                                    </div>
                                    <p class="lead"> <?= $anuncioListDest->descricao  ?>.</p>
                                </div>
                                <div class="col-md-5 order-md-1">
                                    <?php $imagem = $this->Html->image(
                                        '../files/anuncio/' . $anuncioListDest->id . '/' . $anuncioListDest->imagem,
                                        ['class' => 'bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto']
                                    );
                                    echo $this->Html->link(__($imagem), ['controller' => 'Anuncios', 'action' => 'view', $anuncioListDest->slug], ['escape' => false]);
                                    ?>

                                </div>
                            </div>

                        <?php } ?>
                    <?php } ?>
                </div>
                <aside class="col-md-4">
                    <div class="card shadow p-0 mb-3 bg-white">
                        <div class="card-header">
                            Últimos Anúncios da Categoria
                        </div>
                        <div class="p-3">
                            <?php foreach ($anunciosUltimos as $anuncioUltimo) { ?>
                                <li class="media mb-2">
                                    <?php $imagem = $this->Html->image(
                                        '../files/anuncio/' . $anuncioUltimo->id . '/' . $anuncioUltimo->imagem,
                                        ['class' => 'mr-3', 'width' => '64',  'height' => '64', 'alt' => $anuncioUltimo->imagem]
                                    );
                                    echo $this->Html->link(__($imagem), ['controller' => 'Anuncios', 'action' => 'view', $anuncioUltimo->slug], ['escape' => false]);
                                    ?>
                                    <div class="media-body anunc-title">
                                        <?= $this->Html->link(__($anuncioUltimo->titulo), ['controller' => 'Anuncios', 'action' => 'view', $anuncioUltimo->slug]); ?>
                                    </div>
                                </li>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="card shadow p-0 mb-3 bg-white">
                        <div class="card-header">
                            Anúncios em Destaques
                        </div>
                        <div class="p-3">
                            <?php foreach ($anunciosDestaques as $anuncioDestaque) { ?>
                                <li class="media mb-2">
                                    <?php $imagem = $this->Html->image(
                                        '../files/anuncio/' . $anuncioDestaque->id . '/' . $anuncioDestaque->imagem,
                                        ['class' => 'mr-3', 'width' => '64',  'height' => '64', 'alt' => $anuncioDestaque->imagem]
                                    );
                                    echo $this->Html->link(__($imagem), ['controller' => 'Anuncios', 'action' => 'view', $anuncioDestaque->slug], ['escape' => false]);
                                    ?>
                                    <div class="media-body anunc-title">
                                        <?= $this->Html->link(__($anuncioDestaque->titulo), ['controller' => 'Anuncios', 'action' => 'view', $anuncioDestaque->slug]); ?>
                                    </div>
                                </li>
                            <?php } ?>


                        </div>
                    </div>


                </aside>
            </div>
        </div>
    </div>

</main>