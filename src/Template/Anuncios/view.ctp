<main role="main">
    <div class="jumbotron blog">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <?php if ($anuncio) { ?>

                        <div class="blog-post">
                            <h2 class="blog-post-title"><?= $anuncio->titulo ?></h2>

                            <?php
                            setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');

                            date_default_timezone_set('America/Sao_Paulo');
                            $data = date_format($anuncio->created, 'Y-m-d H:i:s');

                            echo  "<p class='blog-post-meta'>Inserido em: " . strftime('%e de %B de %Y às %H:%M', strtotime($data)) . "</p>";
                            ?>
                            <div class="col-md-5 order-md-1">
                                <?= $this->Html->image(
                                    '../files/anuncio/' . $anuncio->id . '/' . $anuncio->imagem,
                                    ['class' => 'bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto']
                                ); ?>

                            </div>

                            <?= $anuncio->conteudo; ?>
                        </div>
                    <?php  } ?>
                </div>


                <aside class="col-md-4">
                    <div class="card p-0 mb-3 bg-white">
                        <div class="card-header">

                            Contatar o anunciante:
                            <?php
                            if (!empty($anunciante->nome)) {
                                echo $anunciante->nome;
                            }
                            ?>
                        </div>
                        <div class="p-3">
                            <p class="lead">Ao ligar, diga que você viu esse anúncio no nosso site</p>
                            <?php
                            if (!empty($anunciante->telefone)) {
                                echo "Telefone Fixo: " . $anunciante->telefone . "<br>";
                            }
                            if (!empty($anunciante->celular)) {
                                echo "Telefone Celular: " . $anunciante->celular . "<br>";
                            } ?>

                            <hr>
                            <?= $this->Flash->render() ?>
                            <?= $this->Form->create($contatosAnunciant) ?>
                            <?= $this->Form->hidden('anuncio_id', [ 'value' => $anuncio->id, 'label' => false]); ?>
                            <?= $this->Form->hidden('anunciant_id', [ 'value' => $anunciante->id, 'label' => false]); ?>



                            <div class="form-group">
                                <?= $this->Form->control('nome', ['class' => 'form-control', 'placeholder' => 'Nome Completo', 'label' => false]); ?>
                            </div>
                            <div class="form-group">
                                <?= $this->Form->control('email', ['class' => 'form-control', 'placeholder' => 'Email', 'label' => false]); ?>
                            </div>
                            <div class="form-group">
                                <?= $this->Form->control('telefone', ['class' => 'form-control', 'placeholder' => 'Telefone', 'label' => false, 
                                'onkeypress'=>"$(this).mask('(00) 0000-00009')" ]); ?>
                            </div>
                            <div class="form-group">
                                <?= $this->Form->control('mensagem', ['type' => 'textarea', 'class' => 'form-control', 'placeholder' => 'Mensagem', 'label' => false]); ?>
                            </div>

                            <?= $this->Form->button(__('Enviar'), ['class' => 'btn btn-info btn-block']) ?>

                            <?= $this->Form->end() ?>

                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>


</main>