<div class="d-flex">
    <div class="mr-auto p-2">
        <h2 class="display-4 titulo">Listar Categorias de Anúncio Destaque</h2>
    </div>
    <div class="p-2">

        <span class="d-none d-md-block">
            <?= $this->Html->link(
                __('Cadastrar'),
                ['controller' => 'CatsAnuncios', 'action' => 'add'],
                ['class' => 'btn btn-outline-success btn-sm']
            )  ?>
            <?= $this->Html->link(
                __('Listar'),
                ['controller' => 'CatsAnuncios', 'action' => 'index'],
                ['class' => 'btn btn-outline-info btn-sm']
            )  ?>

        </span>
        <div class="dropdown d-block d-md-none">
            <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="acoesListar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Ações
            </button>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="acoesListar">
                <?= $this->Html->link(
                    __('Cadastrar'),
                    ['controller' => 'CatsAnuncios', 'action' => 'add'],
                    ['class' => 'dropdown-item']
                )  ?>
                <?= $this->Html->link(
                    __('Listar'),
                    ['controller' => 'CatsAnuncios', 'action' => 'index'],
                    ['class' => 'dropdown-item']
                )  ?>

            </div>
        </div>

    </div>
</div>

<?= $this->Flash->render() ?>


<div class="table-responsive">
    <table class="table table-striped table-hover table-bordered">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th class="d-none d-sm-table-cell">Situação</th>

                <th>Destaque Home</th>
                <th class="text-center">Ações</th>
            </tr>
        </thead>

        <tbody>
            <?php foreach ($catsAnuncioDestaques as $catsAnuncioDestaque) : ?>
                <tr>
                    <td><?= $this->Number->format($catsAnuncioDestaque->id) ?></td>
                    <td><?= h($catsAnuncioDestaque->nome) ?></td>
                    <td>
                        <?php
                        echo "<span class='badge badge-" . $catsAnuncioDestaque->situation->color->cor . "'>
                            " . $catsAnuncioDestaque->situation->nome_situacao . "</span>";

                        ?>
                    </td>

                    <td><?php
                        if ($catsAnuncioDestaque->destaque_home == 1) {
                            $badgeDestaque =  "<span class='badge badge-success'>SIM</span>";

                            echo $this->Html->link(__($badgeDestaque), ['controller' => 'CatsAnuncios', 'action' => 'altCatDestHome', $catsAnuncioDestaque->id], ['escape' => false]);
                        } else {
                            $badgeDestaque = "<span class='badge badge-danger'>NÃO</span>";
                            echo $this->Html->link(__($badgeDestaque), ['controller' => 'CatsAnuncios', 'action' => 'altCatDestHome', $catsAnuncioDestaque->id], ['escape' => false]);
                        }

                        ?></td>
                    <td class="text-center">
                        <span class="d-none d-md-block">
                            <?= $this->Html->link(__('Visualizar'), ['controller' => 'CatsAnuncios', 'action' => 'view', $catsAnuncioDestaque->id], ['class' => 'btn btn-outline-primary btn-sm']) ?>
                            <?= $this->Html->link(__('Editar'), ['controller' => 'CatsAnuncios', 'action' => 'edit', $catsAnuncioDestaque->id], ['class' => 'btn btn-outline-warning btn-sm']) ?>
                            <?= $this->Form->postLink(__('Apagar'), ['controller' => 'CatsAnuncios', 'action' => 'delete', $catsAnuncioDestaque->id], ['class' => 'btn btn-outline-danger btn-sm', 'confirm' => __('Realmente deseja apagar o catsAnuncio  # {0}?', $catsAnuncioDestaque->id)]) ?>
                        </span>
                        <div class="dropdown d-block d-md-none">
                            <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="acoesListar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Ações
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="acoesListar">
                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'CatsAnuncios', 'action' => 'view', $catsAnuncioDestaque->id], ['class' => 'dropdown-item']) ?>
                                <?= $this->Html->link(__('Editar'), ['controller' => 'CatsAnuncios', 'action' => 'edit', $catsAnuncioDestaque->id], ['class' => 'dropdown-item']) ?>
                                <?= $this->Form->postLink(__('Apagar'), ['controller' => 'CatsAnuncios', 'action' => 'delete', $catsAnuncioDestaque->id], ['class' => 'dropdown-item', 'confirm' => __('Realmente deseja apagar o catsAnuncio  # {0}?', $catsAnuncioDestaque->id)]) ?>
                            </div>
                        </div>
                    </td>


                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

</div>