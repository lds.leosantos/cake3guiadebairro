<div class="d-flex">
    <div class="mr-auto p-2">
        <h2 class="display-4 titulo">Editar Anunciante</h2>
    </div>
    <div class="p-2">
        <span class="d-none d-md-block">
            <?= $this->Html->link(
                __('Listar'),
                ['controller' => 'Anunciants', 'action' => 'index'],
                ['class' => 'btn btn-outline-info btn-sm']
            ) ?>
            <?= $this->Html->link(
                __('Visualizar'),
                ['controller' => 'Anunciants', 'action' => 'view', $anunciant->id],
                ['class' => 'btn btn-outline-primary btn-sm']
            ) ?>

        </span>
        <div class="dropdown d-block d-md-none">
            <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="acoesListar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Ações
            </button>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="acoesListar">
                <?= $this->Html->link(__('Listar'), ['controller' => 'Anunciants', 'action' => 'index'], ['class' => 'dropdown-item']) ?>
            </div>
        </div>
    </div>
</div>
<hr>

<?= $this->Flash->render() ?>

<?= $this->Form->create($anunciant, ['enctype' => 'multipart/form-data']) ?>


<div class="form-row">

    <div class="form-group col-md-12">
        <label> Nome</label>
        <?= $this->Form->control('nome', ['class' => 'form-control', 'placeholder' => 'Titulo do Anúncio', 'label' => false]) ?>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-12">
        <label> Descrição Curta</label>
        <?= $this->Form->control('descricao', ['class' => 'form-control', 'placeholder' => 'Descrição do Anúncio', 'label' => false]) ?>
    </div>
</div>

<div class="form-row">
    <div class="form-group col-md-6">
        <label> Slug</label>
        <?= $this->Form->control('slug', ['class' => 'form-control', 'placeholder' => 'Descrição do Anúncio', 'label' => false]) ?>
    </div>
    <div class="form-group col-md-6">
        <label> Palavra Chave</label>
        <?= $this->Form->control('keywords', ['class' => 'form-control', 'placeholder' => 'Descrição do Anúncio', 'label' => false]) ?>
    </div>
</div>

<div class="form-row">

    <div class="form-group col-md-12">
        <label>Resumo do Anunciante</label>
        <?= $this->Form->control('description', ['class' => 'form-control', 'placeholder' => 'Descrição do Anúncio', 'label' => false]) ?>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-6">
        <label>Telefone Fixo</label>
        <?= $this->Form->control('telefone', ['class' => 'form-control', 'id' => 'telefone', 'placeholder' => 'Descrição do Anúncio', 'label' => false]) ?>
    </div>
    <div class="form-group col-md-6">
        <label>Celular</label>
        <?= $this->Form->control('celular', ['class' => 'form-control', 'id' => 'celular', 'placeholder' => 'Descrição do Anúncio', 'label' => false]) ?>
    </div>
</div>
<div class="form-row">

    <div class="form-group col-md-12">
        <label>E-mail</label>
        <?= $this->Form->control('email', ['class' => 'form-control', 'placeholder' => 'Descrição do Anúncio', 'label' => false]) ?>
    </div>
</div>
<?= $this->Html->script(['jquery.maskedinput']) ?>
<div class="form-row">

    <div class="form-group col-md-12">
        <label>Usuário</label>
        <?= $this->Form->control('user_id', ['options' => $users, 'class' => 'form-control', 'placeholder' => 'Descrição do Anúncio', 'label' => false]) ?>
    </div>
</div>

<p>
    <span class="text-danger">* </span>Campo obrigatório
</p>
<?= $this->Form->button(__('Editar'), ['class' => 'btn btn-warning'])  ?>


<?= $this->Form->end() ?>

<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('#celular').mask('(99) 99999 - 9999');
    });

    jQuery(document).ready(function($) {
        $('#telefone').mask('(99) 99999 - 9999');
    });
</script>