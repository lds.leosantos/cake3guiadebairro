<?php

?>
<?= $this->Html->script(['jquery.maskedinput']) ?>
<div class="d-flex">
    <div class="mr-auto p-2">
        <h2 class="display-4 titulo">Cadastrar Informações do Anunciante</h2>
    </div>

</div>
<hr>
<?=  $this->Flash->render()?>

<?= $this->Form->create($anunciant, ['enctype' => 'multipart/form-data']) ?>
<div class="form-row">

    <div class="form-group col-md-12">
        <label> Nome</label>
        <?= $this->Form->control('nome', ['class' => 'form-control', 'placeholder' => 'Nome do Anunciante', 'label' => false]) ?>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-12">
        <label> Descrição Curta</label>
        <?= $this->Form->control('descricao', ['class' => 'form-control', 'placeholder' => 'Descrição do Anunciante', 'label' => false]) ?>
    </div>
</div>

<div class="form-row">
    <div class="form-group col-md-6">
        <label> Slug</label>
        <?= $this->Form->control('slug', ['class' => 'form-control', 'placeholder' => 'slug do Anunciante', 'label' => false]) ?>
    </div>
    <div class="form-group col-md-6">
        <label> Palavra Chave</label>
        <?= $this->Form->control('keywords', ['class' => 'form-control', 'placeholder' => 'Palavra chave do Anunciante', 'label' => false]) ?>
    </div>
</div>

<div class="form-row">

    <div class="form-group col-md-12">
        <label>Resumo do Anunciante</label>
        <?= $this->Form->control('description', ['class' => 'form-control', 'placeholder' => 'Resumo do Anunciante', 'label' => false]) ?>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-6">
        <label><span class="text-danger">*</span> Foto (1920x846)</label>
        <?= $this->Form->control('imagem', ['type' => 'file', 'label' => false, 'onchange' => 'previewImagem()']) ?>
    </div>
    <div class="form-group col-md-6">
        <?php
        $imagem_antiga = '../../files/anunciante/preview_img.jpg';
        ?>
        <img src='<?= $imagem_antiga ?>' alt='Preview da Imagem' id='preview-img' class='img-thumbnail' style="width: 450px; height: 220px;">
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-6">
        <label>Telefone Fixo</label>
        <?= $this->Form->control('telefone', ['class' => 'form-control font-weight-bold', 'id' => 'telefone_pes', 'placeholder' => 'Ex.: (DD) 1234-5678 ', 'label' => false]) ?>
    </div>

    <div class="form-group col-md-6">
        <label>Celular</label>
        <?= $this->Form->control('celular', ['class' => 'form-control', 'id' => 'celular_pes', 'placeholder' => 'Celular do Anunciante', 'label' => false]) ?>
    </div>
</div>
<div class="form-row">

    <div class="form-group col-md-12">
        <label>E-mail</label>
        <?= $this->Form->control('email', ['class' => 'form-control', 'placeholder' => 'Email do Anunciante', 'label' => false]) ?>
    </div>
</div>




<p>
    <span class="text-danger">* </span>Campo obrigatórios
</p>
<?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-success'])  ?>


<?= $this->Form->end() ?>


<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('#telefone_pes').mask('(99) 9999 - 9999');
    });
    jQuery(document).ready(function($) {
        $('#celular_pes').mask('(99) 9999 - 9999');
    });
</script>