<div class="d-flex">
    <div class="mr-auto p-2">
        <h2 class="display-4 titulo">Cadastrar Promoção</h2>
    </div>
    <div class="p-2">
        <span class="d-none d-md-block">
            <?= $this->Html->link(
                __('Listar'),
                ['controller' => 'Promocaos', 'action' => 'index'],
                ['class' => 'btn btn-outline-info btn-sm']
            ) ?>
        </span>
        <div class="dropdown d-block d-md-none">
            <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="acoesListar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Ações
            </button>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="acoesListar">
                <?= $this->Html->link(__('Listar'), ['controller' => 'Promocaos', 'action' => 'index'], ['class' => 'dropdown-item']) ?>
            </div>
        </div>
    </div>
</div>
<hr>









<?= $this->Form->create($promocao, ['enctype' => 'multipart/form-data']) ?>


<div class="form-row">

    <div class="form-group col-md-12">
        <label> Titulo da Promoção</label>
        <?= $this->Form->control('titulo', ['class' => 'form-control', 'placeholder' => 'Titulo do Promoção', 'label' => false]) ?>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-12">
        <label> Descrição Curta Promoção</label>
        <?= $this->Form->control('descricao', ['class' => 'form-control', 'placeholder' => 'Descrição do Promoção', 'label' => false]) ?>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-12">
        <label> Conteúdo Promoção</label>
        <?= $this->Form->control('conteudo', ['class' => 'form-control trumbowyg-um', 'placeholder' => 'Conteúdo do Promoção', 'label' => false]) ?>
    </div>
</div>


<div class="form-row">
    <div class="form-group col-md-6">
        <label><span class="text-danger">*</span> Slug da Promoção</label>
        <?= $this->Form->control('slug', ['class' => 'form-control', 'placeholder' => 'Titulo do Promoção na URL', 'label' => false]) ?>
    </div>
    <div class="form-group col-md-6">
        <label><span class="text-danger">*</span>Palavra Chave Promoção</label>
        <?= $this->Form->control('keywords', ['class' => 'form-control', 'placeholder' => 'Principais Palavras do Promoção para os buscadores', 'label' => false]) ?>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-12">
        <label><span class="text-danger">*</span> Resumo do Promoção</label>
        <?= $this->Form->control('description', ['class' => 'form-control', 'placeholder' => 'Resumo do Promoção - Máximo 180 Caracteres', 'label' => false]) ?>
    </div>
</div>
<div class="form-row">
 
    <div class="form-group col-md-12">
        <label><span class="text-danger">*</span>Situação do Promoção</label>
        <?= $this->Form->control('situation_id', ['options' => $situations, 'class' => 'form-control', 'label' => false]) ?>
    </div>
</div>
<p>
    <span class="text-danger">* </span>Campo obrigatório
</p>
<?= $this->Form->button(__('Cadastrar'), ['class' => 'btn btn-success'])  ?>


<?= $this->Form->end() ?>
