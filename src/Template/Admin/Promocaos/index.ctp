<div class="d-flex">
    <div class="mr-auto p-2">
        <h2 class="display-4 titulo">Listar Promoção</h2>
    </div>
    <div class="p-2">
        <?= $this->Html->link(
            __('Cadastrar'),
            ['controller' => 'Promocaos', 'action' => 'add'],
            ['class' => 'btn btn-outline-success btn-sm']
        ) ?>
    </div>
</div>

<?= $this->Flash->render() ?>


<div class="table-responsive">
    <table class="table table-striped table-hover table-bordered">
        <thead>
            <tr>
                <th>ID</th>
                <th>Título</th>
                <th class="d-none d-sm-table-cell">Situação</th>
                <th class="d-none d-sm-table-cell">Acessos</th>
                <th class="d-none d-lg-table-cell">Data do Cadastro</th>
                <th class="text-center">Ações</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($promocaos as $promocao) : ?>
                <tr>
                    <td><?= $this->Number->format($promocao->id) ?></td>
                    <td><?= h($promocao->titulo) ?></td>
                    <td>

                        <?php

                        echo "<span class='badge badge-" . $promocao->situation->color->cor . "'>
                               " . $promocao->situation->nome_situacao . "</span>";

                        ?>
                    </td>

                    <td><?= $this->Number->format($promocao->qnt_acesso) ?></td>
                    <td><?= h($promocao->created) ?></td>
                    <td class="text-center">
                        <span class="d-none d-md-block">
                            <?= $this->Html->link(__('Visualizar'), ['controller' => 'Promocaos', 'action' => 'view', $promocao->id], ['class' => 'btn btn-outline-primary btn-sm']) ?>
                            <?= $this->Html->link(__('Editar'), ['controller' => 'Promocaos', 'action' => 'edit', $promocao->id], ['class' => 'btn btn-outline-warning btn-sm']) ?>
                            <?= $this->Form->postLink(__('Apagar'), ['controller' => 'Promocaos', 'action' => 'delete', $promocao->id], ['class' => 'btn btn-outline-danger btn-sm', 'confirm' => __('Realmente deseja apagar o promocao  # {0}?', $promocao->id)]) ?>
                        </span>
                        <div class="dropdown d-block d-md-none">
                            <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="acoesListar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Ações
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="acoesListar">
                                <?= $this->Html->link(__('Visualizar'), ['controller' => 'Promocaos', 'action' => 'view', $promocao->id], ['class' => 'dropdown-item']) ?>
                                <?= $this->Html->link(__('Editar'), ['controller' => 'Promocaos', 'action' => 'edit', $promocao->id], ['class' => 'dropdown-item']) ?>
                                <?= $this->Form->postLink(__('Apagar'), ['controller' => 'Promocaos', 'action' => 'delete', $promocao->id], ['class' => 'dropdown-item', 'confirm' => __('Realmente deseja apagar o anuncio  # {0}?', $promocao->id)]) ?>
                            </div>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?= $this->element('pagination'); ?>

</div>