<div class="d-flex">
    <div class="mr-auto p-2">
        <h2 class="display-4 titulo">Alterar Foto do Promoção</h2>
    </div>
    <div class="p-2">
            <span class="d-none d-md-block">
                <?= $this->Html->link(__('Visualizar'), ['controller'=>'Promocaos','action'=>'view',$promocao->id],
                    ['class'=>'btn btn-outline-primary btn-sm']) ?>
            </span>
        <div class="dropdown d-block d-md-none">
            <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="acoesListar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Ações
            </button>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="acoesListar">
                <?= $this->Html->link(__('Visualizar'), ['controller'=>'Promocaos','action'=>'view', $promocao->id],
                    ['class'=>'dropdown-item'])  ?>
            </div>
        </div>
    </div>
</div><hr>
<?= $this->Flash->render() ?>


<?= $this->Form->create($promocao,['enctype' => 'multipart/form-data']) ?>
<div class="form-row">
    <div class="form-group col-md-6">
        <label><span class="text-danger">*</span> Foto (948x481)</label>
        <?= $this->Form->control('imagem', ['type'=>'file','label'=>false, 'onchange'=>'previewImagem()' ]) ?>
    </div>
    <div class="form-group col-md-6">
        <?php
        if ($promocao->imagem !==null){
            $imagem_antiga = '../../../files/promocao/' . $promocao->id .'/'.$promocao->imagem;
        }else{
            $imagem_antiga = '../../../files/promocao/preview_img.png';
        }
        ?>
        <img src='<?= $imagem_antiga?>' alt='<?= $promocao->imagem?>' class="img-thumbnail" id="preview-img" style="width: 250"; height="127">
    </div>
</div>
<p>
    <span class="text-danger">* </span>Campo obrigatório
</p>
<?= $this->Form->button(__('Salvar'), ['class'=>'btn btn-warning'])  ?>
<?= $this->Form->end() ?>

