
<div class="d-flex">
    <div class="mr-auto p-2">
        <h2 class="display-4 titulo">Carousel</h2>
    </div>
    <div class="p-2">
        <span class="d-none d-md-block">
            <?= $this->Html->link(__('Listar'), ['controller'=>'Carousels','action'=>'index'],
                ['class'=>'btn btn-outline-info btn-sm'])  ?>
            <?= $this->Html->link(__('Editar'), ['controller'=>'Carousels','action'=>'edit',$carousel->id],
                ['class'=>'btn btn-outline-warning btn-sm'])  ?>
            <?= $this->Form->postLink(__('Apagar'), ['controller'=>'Carousels','action'=>'delete',$carousel->id],
                ['class'=>'btn btn-outline-danger btn-sm',
                    'confirm' => __('Realmente Deseja Apagar o Carousel ? # {0}?',$carousel->id)]);  ?>
        </span>
        <div class="dropdown d-block d-md-none">
            <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="acoesListar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Ações
            </button>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="acoesListar">
                <?= $this->Html->link(__('Listar'), ['controller'=>'Carousels','action'=>'index'],
                    ['class'=>'dropdown-item'])  ?>
                <?= $this->Html->link(__('Editar'), ['controller'=>'Carousels','action'=>'edit',$carousel->id],
                    ['class'=>'dropdown-item'])  ?>
                <?= $this->Form->postLink(__('Apagar'), ['controller'=>'Carousels','action'=>'delete',$carousel->id],
                    ['class'=>'dropdown-item',
                        'confirm' => __('Realmente Deseja Apagar o Usúario ? # {0}?',$carousel->id)]);  ?>
            </div>
        </div>
    </div>
</div><hr>
<?= $this->Flash->render() ?>
<dl class="row">
    <dt class="col-sm-3">Foto:</dt>
    <dd class="col-sm-9">
        <?php if(!empty($carousel->imagem)){ ?>
            <?= $this->html->image('../files/carousel/'.$carousel->id.'/'. $carousel->imagem,
                ['class'=>'img-fluid','width'=>'420', 'height'=>'520' ])?>&nbsp;
        <?php } else{?>
            <?= $this->html->image('../files/carousel/preview_img.jpg',
                ['class'=>'img-fluid','width'=>'420', 'height'=>'520' ])?>&nbsp;
        <?php } ?>
        <?= $this->Html->link(__('Alterar Foto'),['controller'=>'Carousels','action'=>'alterarFotoCarousel',
            $carousel->id],
            ['class'=>'btn btn-outline-primary btn-sm'])  ?>
    </dd>

    <dt class="col-sm-3">ID:</dt>
    <dd class="col-sm-9"><?= $this->Number->format($carousel->id) ?></dd>

    <dt class="col-sm-3">Nome:</dt>
    <dd class="col-sm-9"><?= h($carousel->nome_carousel) ?>
    </dd>

    <dt class="col-sm-3">Titulo:</dt>
    <dd class="col-sm-9"><?= h($carousel->titulo) ?></dd>

    <dt class="col-sm-3">Descrição:</dt>
    <dd class="col-sm-9"><?= h($carousel->descricao) ?></dd>

    <dt class="col-sm-3">Titulo do Botão:</dt>
    <dd class="col-sm-9"><?= h($carousel->titulo_botao) ?></dd>

    <dt class="col-sm-3">Link do Botão:</dt>
    <dd class="col-sm-9"><?= h($carousel->link) ?></dd>

    <dt class="col-sm-3">Cor do Botão:</dt>
    <dd class="col-sm-9">
        <button type="button" class="btn btn-<?= h($carousel->color->cor) ?> btn-sm">
            <?= h($carousel->color->nome_cor) ?>
        </button>
    </dd>
    <dt class="col-sm-3">Posição do Texto:</dt>
    <dd class="col-sm-9"><?= h($carousel->position->nome_posicao) ?></dd>

    <dt class="col-sm-3">Ordem do Slide:</dt>
    <dd class="col-sm-9"><?= h($carousel->ordem) ?></dd>

    <dt class="col-sm-3">Situação do Slide:</dt>
    <dd class="col-sm-9">
        <?php
        if ($carousel->situation->id == 1){
            echo "<span class='badge badge-success'>
            ".$carousel->situation->nome_situacao."</span>";
        }else{
            echo "<span class='badge badge-danger'>
            ".$carousel->situation->nome_situacao."</span>";
        }
        ?>
    </dd>

    <dt class="col-sm-3 text-truncate">Data do Cadastro:</dt>
    <dd class="col-sm-9"><?= h($carousel->created) ?></dd>

    <dt class="col-sm-3 text-truncate">Data de Alteração:</dt>
    <dd class="col-sm-9"><?= h($carousel->modified) ?></dd>
</dl>
