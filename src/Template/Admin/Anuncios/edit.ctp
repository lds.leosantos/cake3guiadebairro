<div class="d-flex">
    <div class="mr-auto p-2">
        <h2 class="display-4 titulo">Cadastrar Anúncio</h2>
    </div>
    <div class="p-2">
        <span class="d-none d-md-block">
            <?= $this->Html->link(
                __('Listar'),
                ['controller' => 'Anuncios', 'action' => 'index'],
                ['class' => 'btn btn-outline-info btn-sm']
            ) ?>
        </span>
        <div class="dropdown d-block d-md-none">
            <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="acoesListar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Ações
            </button>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="acoesListar">
                <?= $this->Html->link(__('Listar'), ['controller' => 'Anuncios', 'action' => 'index'], ['class' => 'dropdown-item']) ?>
            </div>
        </div>
    </div>
</div>
<hr>



<?= $this->Form->create($anuncio, ['enctype' => 'multipart/form-data']) ?>


<div class="form-row">

    <div class="form-group col-md-12">
        <label> Titulo</label>
        <?= $this->Form->control('titulo', ['class' => 'form-control', 'placeholder' => 'Titulo do Anúncio', 'label' => false]) ?>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-12">
        <label> Descrição Curta</label>
        <?= $this->Form->control('descricao', ['class' => 'form-control', 'placeholder' => 'Descrição do Anúncio', 'label' => false]) ?>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-12">
        <label> Conteúdo</label>
        <?= $this->Form->control('conteudo', ['class' => 'form-control trumbowyg-um', 'placeholder' => 'Conteúdo do Anúncio', 'label' => false]) ?>
    </div>
</div>


<div class="form-row">
    <div class="form-group col-md-6">
        <label><span class="text-danger">*</span> Slug</label>
        <?= $this->Form->control('slug', ['class' => 'form-control', 'placeholder' => 'Titulo do anuncio na URL', 'label' => false]) ?>
    </div>
    <div class="form-group col-md-6">
        <label><span class="text-danger">*</span>Palavra Chave</label>
        <?= $this->Form->control('keywords', ['class' => 'form-control', 'placeholder' => 'Principais Palavras do anúncio para os buscadores', 'label' => false]) ?>
    </div>


</div>

<div class="form-row">
    <div class="form-group col-md-12">
        <label><span class="text-danger">*</span> Resumo do Anúncio</label>
        <?= $this->Form->control('description', ['class' => 'form-control', 'placeholder' => 'Resumo do Anúncio - Máximo 180 Caracteres', 'label' => false]) ?>
    </div>
</div>


<div class="form-row">
 
    <div class="form-group col-md-6">
        <label><span class="text-danger">*</span>Situação do Anuncio</label>
        <?= $this->Form->control('anuncios_situation_id', ['options' => $anunciosSituations, 'class' => 'form-control', 'placeholder' => 'Link do botão', 'label' => false]) ?>
    </div>
    <div class="form-group col-md-6">
        <label><span class="text-danger">*</span> Categoria do Anúncio</label>
        <?= $this->Form->control('cats_anuncio_id', ['options' => $catsAnuncios, 'class' => 'form-control', 'placeholder' => 'Titulo do Botão', 'label' => false]) ?>
    </div>
</div>
<p>
    <span class="text-danger">* </span>Campo obrigatório
</p>
<?= $this->Form->button(__('Editar'), ['class' => 'btn btn-warning'])  ?>


<?= $this->Form->end() ?>
