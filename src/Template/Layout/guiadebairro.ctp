<?php


$cakeDescription = 'Guia de Bairro';
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css(['bootstrap.min','pers_guia_bairro','fontawesome.min']) ?>
    <?= $this->Html->script(['all.min']) ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
        <?= $this->element('menu_guia_bairro'); ?>
        <?= $this->fetch('content') ?>
        <?= $this->element('rodape_guia_bairro'); ?>

    <?= $this->Html->script(['jquery-3.3.1.min','popper.min','bootstrap.min.js','jquery.mask.min']) ?>
</body>
</html>
