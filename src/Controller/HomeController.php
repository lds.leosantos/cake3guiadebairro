<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\Routing\Router;



class HomeController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['index']);
    }

    public function index()
    {
        $carouselTable = TableRegistry::getTableLocator()->get('Carousels');
        $carousels = $carouselTable->getListarSlidesHome();

        $catsAnunciosTable = TableRegistry::getTableLocator()->get('CatsAnuncios');
        $catAnuncios = $catsAnunciosTable->getListarCatAnuncioHome();
        $this->set(compact('carousels','catAnuncios'));
    }
}
