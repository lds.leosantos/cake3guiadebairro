<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\Routing\Router;



class ListAnunciosController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['index']);
    }

    public function index($slug = null)
    {
        $catAnunciosTable = TableRegistry::getTableLocator()->get('CatsAnuncios');
        $catAnuncio  = $catAnunciosTable->getVerCatAnuncio($slug);
        $anunciosListDests = null;
        $anuncios = null;
        $nomecategoria  = $catAnunciosTable->getNomeCategoria($slug);


        if ($catAnuncio) {
            $anuncioTable = TableRegistry::getTableLocator()->get('Anuncios');
            $this->paginate = [
                'limit' => 2,
                'conditions' => [
                    'Anuncios.cats_anuncio_id =' => $catAnuncio->id,
                    'Anuncios.anuncios_situation_id =' => 1,
                ],
                'order' => [
                    'Anuncios.id' => 'DESC'
                ]
            ];

            $anuncios = $this->paginate($anuncioTable);
            $anunciosUltimos = $anuncioTable->getCatAnunciosUltimos($catAnuncio->id);
            $anunciosDestaques = $anuncioTable->getAnunciosDestaque();

            
      
        } else {
            $anuncioTable = TableRegistry::getTableLocator()->get('Anuncios');
            $anunciosUltimos = $anuncioTable->getAnunciosUltimos();
            $anuncioTable = TableRegistry::getTableLocator()->get('Anuncios');
            $anunciosDestaques = $anuncioTable->getAnunciosDestaque();
            $anunciosListDests = $anuncioTable->getListAnunciosDestaque();
        }


        $this->set(compact('catAnuncio','anuncios', 'anunciosUltimos', 'anunciosDestaques', 'anunciosListDests'));
    }
}
