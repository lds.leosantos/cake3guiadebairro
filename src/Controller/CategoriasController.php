<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\Routing\Router;



class CategoriasController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['index']);
    }

    public function index()
    {
      
       
        $catsAnunciosTable = TableRegistry::getTableLocator()->get('CatsAnuncios');
        $catAnuncios = $catsAnunciosTable->getListCatAnuncio();

        $this->set(compact('catAnuncios'));
    }

}