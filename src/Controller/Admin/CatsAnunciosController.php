<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * CatsAnuncios Controller
 *
 * @property \App\Model\Table\CatsAnunciosTable $CatsAnuncios
 *
 * @method \App\Model\Entity\CatsAnuncio[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CatsAnunciosController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Situations', 'Situations.Colors'],
            'order' => ['CatsAnuncios.ordem' => 'ASC']
        ];
        $catsAnuncios = $this->paginate($this->CatsAnuncios);

        $this->set(compact('catsAnuncios'));
    }

    /**
     * View method
     *
     * @param string|null $id Cats Anuncio id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $catsAnuncio = $this->CatsAnuncios->get($id, [
            'contain' => ['Situations', 'Situations.Colors'],
        ]);

        $this->set('catsAnuncio', $catsAnuncio);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $catsAnuncio = $this->CatsAnuncios->newEntity();
        if ($this->request->is('post')) {
            $catsAnuncio = $this->CatsAnuncios->patchEntity($catsAnuncio, $this->request->getData());

            if (!$catsAnuncio->getErrors()) {
                $catsAnuncio->slug = $this->CatsAnuncios->slugUrlSimples($this->request->getData()['nome']);

                $catsAnuncioTable = TableRegistry::getTableLocator()->get('CatsAnuncios');
                $ultimoCatAnuncio = $catsAnuncioTable->getUltimoCatAnuncio();
                $catsAnuncio->ordem = $ultimoCatAnuncio->ordem + 1;

                if ($resultSave = $this->CatsAnuncios->save($catsAnuncio)) {
                    $id = $resultSave->id;

                    $this->Flash->success(__('The cats anuncio has been saved.'));

                    return $this->redirect(['action' => 'view', $id]);
                } else {
                    $this->Flash->danger(__('The cats anuncio could not be saved. Please, try again.'));
                }
            } else {
                $this->Flash->danger(__('The cats anuncio could not be saved. Please, try again.'));
            }
        }
        $situations = $this->CatsAnuncios->Situations->find('list', ['limit' => 200]);
        $this->set(compact('catsAnuncio', 'situations'));
    }

    public function edit($id = null)
    {
        $catsAnuncio = $this->CatsAnuncios->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $catsAnuncio = $this->CatsAnuncios->patchEntity($catsAnuncio, $this->request->getData());

            if (!$catsAnuncio->getErrors()) {
                $catsAnuncio->slug = $this->CatsAnuncios->slugUrlSimples($this->request->getData()['nome']);

                if ($this->CatsAnuncios->save($catsAnuncio)) {
                    $this->Flash->success(__('The cats anuncio has been saved.'));

                    return $this->redirect(['action' => 'view', $id]);
                } else {
                    $this->Flash->danger(__('The cats anuncio could not be saved. Please, try again.'));
                }
            } else {
                $this->Flash->danger(__('The cats anuncio could not be saved. Please, try again.'));
            }
        }
        $situations = $this->CatsAnuncios->Situations->find('list', ['limit' => 200]);
        $this->set(compact('catsAnuncio', 'situations'));
    }


    public function altCatDestHome($id = null)
    {
        $catsAnuncioTable = TableRegistry::getTableLocator()->get('CatsAnuncios');
        $catsAnuncioDestHome = $catsAnuncioTable->getCatDestHome($id);

        if ($catsAnuncioDestHome->destaque_home == 2) {

            $altCatAnuncDestHome = $this->CatsAnuncios->newEntity();
            $altCatAnuncDestHome->destaque_home = 1;
            $altCatAnuncDestHome->id = $catsAnuncioDestHome->id;
            $this->CatsAnuncios->save($altCatAnuncDestHome);

            $this->Flash->success(__('Destaque Home alterado com Sucesso!!'));
            return $this->redirect(['controller' => 'CatsAnuncios', 'action' => 'index']);
        } else {
            $altCatAnuncDestHome = $this->CatsAnuncios->newEntity();
            $altCatAnuncDestHome->destaque_home = 2;
            $altCatAnuncDestHome->id = $catsAnuncioDestHome->id;
            $this->CatsAnuncios->save($altCatAnuncDestHome);

            $this->Flash->success(__('Destaque Home alterado com Sucesso!!'));
            return $this->redirect(['controller' => 'CatsAnuncios', 'action' => 'index']);
        }

        $this->Flash->danger(__('Destaque Home não foi  alterado com Sucesso!!'));
        return $this->redirect(['controller' => 'CatsAnuncios', 'action' => 'index']);
    }

    public function listCatDestaqueHome()
    {
        $catsAnuncioTable = TableRegistry::getTableLocator()->get('CatsAnuncios');
        $catsAnuncioDestaques = $catsAnuncioTable->getListCategoriaAnuncioDestaque();

        $this->set(compact('catsAnuncioDestaques'));
    }

    public function altOrdemCatsAnuncios($id = null)
    {
        $catsAnuncioTable = TableRegistry::getTableLocator()->get('CatsAnuncios');
        $catsAnuncioAtual = $catsAnuncioTable->getCatAnuncioAtual($id);

        $ordemMenor = $catsAnuncioAtual->ordem - 1;

        $catsAnuncioMenor = $catsAnuncioTable->getCatAnuncioMenor($ordemMenor);

        if ($catsAnuncioMenor) {


            $catsAnuncioAtualAlt = $this->CatsAnuncios->newEntity();
            $catsAnuncioAtualAlt->id = $catsAnuncioAtual->id;
            $catsAnuncioAtualAlt->ordem = $catsAnuncioAtual->ordem - 1;

            $this->CatsAnuncios->save($catsAnuncioAtualAlt);


            $catsAnuncioMenorAlt = $this->CatsAnuncios->newEntity();
            $catsAnuncioMenorAlt->id = $catsAnuncioMenor->id;
            $catsAnuncioMenorAlt->ordem = $catsAnuncioMenor->ordem + 1;

            $this->CatsAnuncios->save($catsAnuncioMenorAlt);

            $this->Flash->success(__('Alterada a ordem com Sucesso!!'));
            return $this->redirect(['controller' => 'CatsAnuncios', 'action' => 'index']);
        } else {
            $this->Flash->danger(__('A ordem não foi alterada com Sucesso!!'));
            return $this->redirect(['controller' => 'CatsAnuncios', 'action' => 'index']);
        }
    }
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $catsAnuncio = $this->CatsAnuncios->get($id);


        $catsAnuncioTable = TableRegistry::getTableLocator()->get('CatsAnuncios');
        $ListCatAnuncioProx = $catsAnuncioTable->getListCatAnuncioProx($catsAnuncio->ordem);
        if ($this->CatsAnuncios->delete($catsAnuncio)) {

            foreach ($ListCatAnuncioProx as $CatAnuncioProx) {
                $catsAnuncio->ordem = $CatAnuncioProx->ordem - 1;
                $catsAnuncio->id = $CatAnuncioProx->id;
                $this->CatsAnuncios->save($catsAnuncio);
            }
            $this->Flash->success(__('The cats anuncio has been deleted.'));
        } else {
            $this->Flash->danger(__('The cats anuncio could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
