<?php

namespace App\Controller\Admin;

use App\Controller\AppController;


class AnunciantsController extends AppController
{

    public function index()
    {
        $this->paginate = [
            'contain' => ['Users'],
        ];
        $anunciants = $this->paginate($this->Anunciants);

        $this->set(compact('anunciants'));
    }


    public function view($id = null)
    {
        $anunciant = $this->Anunciants->get($id, [
            'contain' => ['Users'],
        ]);

        $this->set('anunciant', $anunciant);
    }
    public function viewAnunciante($id = null)
    {
        $id_user = $this->Auth->user('id');
        $anunciant = $this->Anunciants->getverAnuncianteAdm($id_user);

        if ($anunciant) {
            $this->set('anunciant', $anunciant);
        } else {
            return $this->redirect(['controller' => 'Anunciants', 'action' => 'addAnunciante']);
        }
    }

    public function addAnunciante()
    {
        $anunciant = $this->Anunciants->newEntity();
        if ($this->request->is('post')) {

            $anunciant = $this->Anunciants->patchEntity($anunciant, $this->request->getData());

            if (!$anunciant->getErrors()) {
                $anunciant->imagem = $this->Anunciants->slugUploadImgRed($this->request->getData()['imagem']['name']);
                $anunciant->slug = $this->Anunciants->slugUrlSimples($this->request->getData()['slug']);
                //id do usário logado
                $anunciant->user_id = $this->Auth->user('id');


                if ($this->Anunciants->save($anunciant)) {

                    $destino = WWW_ROOT . "files" . DS . "anunciante" . DS . $anunciant->id . DS;

                    $imgUpload = $this->request->getData()['imagem'];
                    $imgUpload['name'] = $anunciant->imagem;

                    if ($this->Anunciants->uploadImgRed($imgUpload, $destino, 1820, 846)) {
                        $this->Flash->success(__('Anunciants Cadastrado com Sucesso!!.'));
                        return $this->redirect(['controller' => 'Anunciants', 'action' => 'viewAnunciante']);
                    } else {
                        $this->Flash->danger(__('Imagem não foi cadastrada com sucesso. Erro No Upload. Tente Novamente'));
                    }
                } else {
                    $this->Flash->danger(__('O Anunciants não foi salvo com sucesso. Tente Novamente'));
                }
            }
        }
        $this->set(compact('anunciant'));
    }
    public function add()
    {
        $anunciant = $this->Anunciants->newEntity();
        if ($this->request->is('post')) {

            $anunciant = $this->Anunciants->patchEntity($anunciant, $this->request->getData());

            if (!$anunciant->getErrors()) {
                $anunciant->imagem = $this->Anunciants->slugUploadImgRed($this->request->getData()['imagem']['name']);
                $anunciant->slug = $this->Anunciants->slugUrlSimples($this->request->getData()['slug']);

                if ($this->Anunciants->save($anunciant)) {

                    $destino = WWW_ROOT . "files" . DS . "anunciante" . DS . $anunciant->id . DS;

                    $imgUpload = $this->request->getData()['imagem'];
                    $imgUpload['name'] = $anunciant->imagem;

                    if ($this->Anunciants->uploadImgRed($imgUpload, $destino, 1820, 846)) {
                        $this->Flash->success(__('Anunciants Cadastrado com Sucesso!!.'));
                        return $this->redirect(['controller' => 'Anunciants', 'action' => 'index']);
                    } else {
                        $this->Flash->danger(__('Imagem não foi cadastrada com sucesso. Erro No Upload. Tente Novamente'));
                    }
                } else {
                    $this->Flash->danger(__('O Anunciants não foi salvo com sucesso. Tente Novamente'));
                }
            }
        }
        $users = $this->Anunciants->Users->find('list', ['limit' => 200]);
        $this->set(compact('anunciant', 'users'));
    }
    public function editAnunciante()
    {
        $id_user  = $this->Auth->user('id');
        $anunciant = $this->Anunciants->getEditAnunciantAdm($id_user);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $anunciant = $this->Anunciants->patchEntity($anunciant, $this->request->getData());
            $anunciant->slug = $this->Anunciants->slugUrlSimples($this->request->getData()['slug'] . "-" . $id_user);
            if ($this->Anunciants->save($anunciant)) {
                $this->Flash->success(__('The anunciant has been saved.'));

                return $this->redirect(['action' => 'viewAnunciante']);
            }
            $this->Flash->error(__('The anunciant could not be saved. Please, try again.'));
        }
        $this->set(compact('anunciant'));
    }

    public function edit($id = null)
    {
        $anunciant = $this->Anunciants->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $anunciant = $this->Anunciants->patchEntity($anunciant, $this->request->getData());
            $anunciant->slug = $this->Anunciants->slugUrlSimples($this->request->getData()['slug'] . "-" . $id);

            if ($this->Anunciants->save($anunciant)) {
                $this->Flash->success(__('The anunciant has been saved.'));

                return $this->redirect(['action' => 'view', $id]);
            }
            $this->Flash->error(__('The anunciant could not be saved. Please, try again.'));
        }
        $users = $this->Anunciants->Users->find('list', ['limit' => 200]);
        $this->set(compact('anunciant', 'users'));
    }

    public function alterarFotoAnunciante($id = null)
    {

        $anunciant = $this->Anunciants->get($id);
        $imagemAntiga = $anunciant->imagem;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $anunciant = $this->Anunciants->newEntity();
            $anunciant = $this->Anunciants->patchEntity($anunciant, $this->request->getData());

            if (!$anunciant->getErrors()) {
                $anunciant->imagem = $this->Anunciants->slugUploadImgRed($this->request->getData()['imagem']['name']);
                $anunciant->id = $id;
                if ($this->Anunciants->save($anunciant)) {
                    $destino = WWW_ROOT . "files" . DS . "anunciante" . DS . $id . DS;
                    $imgUpload = $this->request->getData()['imagem'];
                    $imgUpload['name'] = $anunciant->imagem;

                    if ($this->Anunciants->uploadImgRed($imgUpload, $destino, 1820, 846)) {
                        $this->Anunciants->deleteFile($destino, $imagemAntiga, $anunciant->imagem);
                        $this->Flash->success(__('Imagem editada com sucesso'));
                        return $this->redirect(['controller' => 'Anunciants', 'action' => 'view', $id]);
                    } else {
                        $anunciant->imagem = $imagemAntiga;
                        $this->Users->save($anunciant);
                        $this->Flash->danger(__('Erro: Imagem não foi editada com sucesso. Erro ao realizar o upload'));
                    }
                } else {
                    $this->Flash->danger(__('Erro: Imagem não foi editada com sucesso.'));
                }
            } else {
                $this->Flash->danger(__('Erro: Imagem não foi editada com sucesso.'));
            }
        }

        $this->set(compact('anunciant'));
    }


    public function alterarImgAnunciante()
    {

        $user_id  = $this->Auth->user('id');
        $anunciant = $this->Anunciants->getEditImgAnunciante($user_id);
        $imagemAntiga = $anunciant->imagem;
        $id = $anunciant->id;



        if ($this->request->is(['patch', 'post', 'put'])) {
            $anunciant = $this->Anunciants->newEntity();
            $anunciant = $this->Anunciants->patchEntity($anunciant, $this->request->getData());

            if (!$anunciant->getErrors()) {

                $anunciant->imagem = $this->Anunciants->slugUploadImgRed($this->request->getData()['imagem']['name']);
                $anunciant->id = $id;

                if ($this->Anunciants->save($anunciant)) {

                    $destino = WWW_ROOT . "files" . DS . "anunciante" . DS . $id . DS;
                    $imgUpload = $this->request->getData()['imagem'];
                    $imgUpload['name'] = $anunciant->imagem;

                    if ($this->Anunciants->uploadImgRed($imgUpload, $destino, 1820, 846)) {
                        $this->Anunciants->deleteFile($destino, $imagemAntiga, $anunciant->imagem);
                        $this->Flash->success(__('Imagem editada com sucesso'));
                        return $this->redirect(['controller' => 'Anunciants', 'action' => 'viewAnunciante']);
                    } else {
                        $anunciant->imagem = $imagemAntiga;
                        $this->Anunciants->save($anunciant);
                        $this->Flash->danger(__('Erro: Imagem não foi editada com sucesso. Erro ao realizar o upload'));
                    }
                } else {
                    $this->Flash->danger(__('Erro: Imagem não foi editada com sucesso.'));
                }
            } else {
                $this->Flash->danger(__('Erro: Imagem não foi editada com sucesso.'));
            }
        }

        $this->set(compact('anunciant'));
    }
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $anunciant = $this->Anunciants->get($id);

        $destino = WWW_ROOT . "files" . DS . "anunciante" . DS . $id . DS;

        $this->Anunciants->deleteArq($destino);

        if ($this->Anunciants->delete($anunciant)) {
            $this->Flash->success(__('The anunciant has been deleted.'));
        } else {
            $this->Flash->error(__('The anunciant could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
