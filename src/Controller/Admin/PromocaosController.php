<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Promocaos Controller
 *
 * @property \App\Model\Table\PromocaosTable $Promocaos
 *
 * @method \App\Model\Entity\Promocao[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PromocaosController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Robots', 'Users', 'Situations', 'Situations.Colors'],
        ];
        $promocaos = $this->paginate($this->Promocaos);


        $this->set(compact('promocaos'));
    }

    /**
     * View method
     *
     * @param string|null $id Promocao id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $promocao = $this->Promocaos->get($id, [
            'contain' => ['Robots', 'Users', 'Situations', 'Situations.Colors'],
        ]);

        $this->set('promocao', $promocao);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        // $promocao = $this->Promocaos->newEntity();
        // if ($this->request->is('post')) {
        //     $promocao = $this->Promocaos->patchEntity($promocao, $this->request->getData());
        //     if ($this->Promocaos->save($promocao)) {
        //         $this->Flash->success(__('The promocao has been saved.'));

        //         return $this->redirect(['action' => 'index']);
        //     }
        //     $this->Flash->error(__('The promocao could not be saved. Please, try again.'));
        // }

        $promocao = $this->Promocaos->newEntity();
        if ($this->request->is('post')) {
            $promocao = $this->Promocaos->patchEntity($promocao, $this->request->getData());
            if (!$promocao->getErrors()) {

                $promocao->imagem = $this->Promocaos->slugUploadImgRed($this->request->getData()['imagem']['name']);
                $promocao->slug = $this->Promocaos->slugUrlSimples($this->request->getData()['slug']);
                $promocao->robot_id = 1;
                $promocao->user_id = $this->Auth->user('id');

                if ($this->Promocaos->save($promocao)) {

                    $promocao->slug = $this->Promocaos->slugUrlSimples($this->request->getData()['slug'] . "-" . $promocao->id);
                    $this->Promocaos->save($promocao);

                    $destino = WWW_ROOT . "files" . DS . "promocao" . DS . $promocao->id . DS;

                    $imgUpload = $this->request->getData()['imagem'];
                    $imgUpload['name'] = $promocao->imagem;

                    if ($this->Promocaos->uploadImgRed($imgUpload, $destino, 948, 481)) {
                        $this->Flash->success(__('The promocao has been saved.'));

                        return $this->redirect(['action' => 'index']);
                    } else {
                        $this->Flash->danger(__('Error de Upload da Imagem: The anuncio could not be saved. Please, try again.'));
                    }
                } else {
                    $this->Flash->danger(__('The promocao could not be saved. Please, try again.'));
                }
            } else {
                $this->Flash->danger(__('Error:The promocao could not be saved. Please, try again.'));
            }
        }
        $robots = $this->Promocaos->Robots->find('list', ['limit' => 200]);
        $users = $this->Promocaos->Users->find('list', ['limit' => 200]);
        $situations = $this->Promocaos->Situations->find('list', ['limit' => 200]);
        $this->set(compact('promocao', 'robots', 'users', 'situations'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Promocao id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $promocao = $this->Promocaos->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $promocao = $this->Promocaos->patchEntity($promocao, $this->request->getData());
            $promocao->slug = $this->Promocaos->slugUrlSimples($this->request->getData()['slug']);

            if ($this->Promocaos->save($promocao)) {
                $this->Flash->success(__('The promocao has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The promocao could not be saved. Please, try again.'));
        }
        $robots = $this->Promocaos->Robots->find('list', ['limit' => 200]);
        $users = $this->Promocaos->Users->find('list', ['limit' => 200]);
        $situations = $this->Promocaos->Situations->find('list', ['limit' => 200]);
        $this->set(compact('promocao', 'robots', 'users', 'situations'));
    }

    public function alterarFotoPromocao($id = null)
    {

                // $promocao = $this->Promocaos->newEntity();

        $promocao = $this->Promocaos->get($id);

        $imagemAntiga = $promocao->imagem;


        if ($this->request->is(['patch', 'post', 'put'])) {
            $promocao = $this->Promocaos->newEntity();
            $promocao = $this->Promocaos->patchEntity($promocao, $this->request->getData());

            if (!$promocao->getErrors()) {
                $promocao->imagem = $this->Promocaos->slugUploadImgRed($this->request->getData()['imagem']['name']);
                $promocao->id = $id;
                if ($this->Promocaos->save($promocao)) {
                    $destino = WWW_ROOT . "files" . DS . "promocao" . DS . $id . DS;
                    $imgUpload = $this->request->getData()['imagem'];
                    $imgUpload['name'] = $promocao->imagem;

                    if ($this->Promocaos->uploadImgRed($imgUpload, $destino, 948, 481)) {
                        $this->Promocaos->deleteFile($destino, $imagemAntiga, $promocao->imagem);
                        $this->Flash->success(__('Imagem editada com sucesso'));
                        return $this->redirect(['controller' => 'Promocaos', 'action' => 'view', $id]);
                    } else {
                        $promocao->imagem = $imagemAntiga;
                        $this->Promocaos->save($promocao);
                        $this->Flash->danger(__('Erro: Imagem não foi editada com sucesso. Erro ao realizar o upload'));
                    }
                } else {
                    $this->Flash->danger(__('Erro: Imagem não foi editada com sucesso.'));
                }
            } else {
                $this->Flash->danger(__('Erro: Imagem não foi editada com sucesso.'));
            }
        }

        $this->set(compact('promocao'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $promocao = $this->Promocaos->get($id);

        $id = $promocao->id;

    
        $destino = WWW_ROOT . "files" . DS . "promocao" . DS . $id . DS;

        $this->Promocaos->deleteArq($destino);
        if ($this->Promocaos->delete($promocao)) {
            $this->Flash->success(__('The promocao has been deleted.'));
        } else {
            $this->Flash->error(__('The promocao could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
