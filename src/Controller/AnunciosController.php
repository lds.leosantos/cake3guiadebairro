<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\Routing\Router;
use Cake\Mailer\Mailer;



class AnunciosController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['view']);
    }

    use MailerAwareTrait;
    public function view($slug = null)
    {

        $ContatosAnunciantsTable = TableRegistry::getTableLocator()->get('ContatosAnunciants');
        $contatosAnunciant =  $ContatosAnunciantsTable->newEntity();

        if ($this->request->is(['post', 'patch', 'put'])) {

            $contatosAnunciant =  $ContatosAnunciantsTable->patchEntity($contatosAnunciant, $this->request->getData());
            if (!$contatosAnunciant->getErrors()) {


                $msgContAnuncTable = TableRegistry::getTableLocator()->get('Anunciants');
                $anunciante = $msgContAnuncTable->getVerAnuncianteCont($contatosAnunciant->anunciant_id);

                $msgContAnunc->nome = $contatosAnunciant->nome;
                $msgContAnunc->emailCliente = $contatosAnunciant->email;
                $msgContAnunc->telefone = $contatosAnunciant->telefone;
                $msgContAnunc->mensagem = $contatosAnunciant->mensagem;



                exit;

                if ($ContatosAnunciantsTable->save($contatosAnunciant)) {
                    $this->getMailer('ContatoAnunciant')->send('msgContatoCliente', [$contatosAnunciant]);
                    $this->getMailer('ContatoAnunciant')->send('msgContatoAnunciante', [$msgContAnunc]);


                    $this->Flash->success(__('Mensagem enviada com sucesso'));
                } else {
                    $this->Flash->danger(__('Error: Mensagem não foi enviada com sucesso.'));
                }
            }
        }

        $anuncioTable = TableRegistry::getTableLocator()->get('Anuncios');
        $anuncio =   $anuncioTable->getVerAnuncio($slug);


        if ($anuncio) {
            $anuncianteTable = TableRegistry::getTableLocator()->get('Anunciants');

            $anunciante = $anuncianteTable->getVerAnunciante($anuncio->user_id);
        }


        $this->set(compact('anuncio', 'anunciante', 'contatosAnunciant'));
    }
}
