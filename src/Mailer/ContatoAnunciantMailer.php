<?php

namespace App\Mailer;

use Cake\Mailer\Mailer;

/**
 * ContatoAnunciant mailer.
 */
class ContatoAnunciantMailer extends Mailer
{
    /**
     * Mailer's name.
     *
     * @var string
     */
    public static $name = 'ContatoAnunciant';

    public function  msgContatoCliente($msgContAnuncCliente)
    {
        $this->setTo($msgContAnuncCliente->email)
            ->setProfile('envemail')
            ->setEmailFormat('html')
            ->setTemplate('msg_cont_cliente')
            ->setLayout('contato_anunciant')
            ->setViewVars(['nome' => $msgContAnuncCliente->nome, 'mensagem' => $msgContAnuncCliente->mensagem])
            ->setSubject(sprintf('mensagem enviada para o anunciante'));
    }

    public function  msgContatoAnunciante($msgContAnunc)
    {
        $this->setTo($msgContAnunc->email)
            ->setProfile('envemail')
            ->setEmailFormat('html')
            ->setTemplate('msg_cont_anunciante')
            ->setLayout('contato_anunciant')
            ->setViewVars([
                'nome' => $msgContAnunc->nome, 'mensagem' => $msgContAnunc->mensagem,
                'email' => $msgContAnunc->emailCliente, 'telefone' => $msgContAnunc->telefone
            ])
            ->setSubject(sprintf('Nova mensagem no seu anuncio'));
    }
}
